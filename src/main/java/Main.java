import control.God;
import model.World;
import model.creature.Player;
import model.creature.template.PlayerTemplate;
import view.SwingView;

public class Main {
    private static final int FIELD_WIDHT = 15;
    private static final int FIELD_HEIGHT = 15;

    public static void main(String[] args) {
        God god = new God();
        World world = god.createWorld(FIELD_WIDHT, FIELD_HEIGHT);
        Player player = god.createPlayer(PlayerTemplate.NORMAL);

        SwingView view = new SwingView();
        view.addCommandListener(player.getAi());
        view.addCommandListener(god);
        view.init(world);
    }
}
