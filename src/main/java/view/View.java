package view;

import model.World;

/**
 * Created by asazonov on 17.07.2018.
 */
public interface View {
    void init(World world);
    void repaint();
}
