package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;

import lombok.Getter;
import lombok.Setter;
import model.World;
import model.WorldObject;
import model.creature.Player;

/**
 * Created by asazonov on 17.07.2018.
 */
public class JWorldView extends JComponent {

    private static final String FONT_NAME = "Consolas";

    @Getter
    @Setter
    private World world;

    private Rectangle2D oneCharBounds = new Rectangle2D.Double();

    private final ComponentListener cl = new ComponentAdapter() {
        @Override
        public void componentResized(ComponentEvent e) {
            int i = 0;
            Rectangle2D bounds;
            do {
                i++;
                Font font = new Font(FONT_NAME, Font.PLAIN, i);
                bounds = getFontMetrics(font).getStringBounds("█", null);
            }
            while (getWidth() >= bounds.getWidth() * world.getWidth() && getHeight() >= bounds.getHeight() * world.getHeight());

            setFont(new Font(FONT_NAME, Font.PLAIN, i - 1));
            oneCharBounds = getFontMetrics(getFont()).getStringBounds("█", null);
        }
    };

    {
        addComponentListener(cl);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.BLACK);
        g.fillRect(0, 0,
                (int) oneCharBounds.getX() + (int) oneCharBounds.getWidth() * world.getWidth(),
                (int) oneCharBounds.getY() + (int) oneCharBounds.getHeight() * world.getHeight());

        for (WorldObject o : world.getObjects()) {
            if (o instanceof Player) {
                g.setColor(Color.YELLOW);
            } else {
                g.setColor(Color.GRAY);
            }
            g.drawString(o.getSkin(),
                    (int) -oneCharBounds.getX() + o.getPosition().getX() * (int) oneCharBounds.getWidth(),
                    (int) -oneCharBounds.getY() + o.getPosition().getY() * (int) oneCharBounds.getHeight());
        }
    }
}
