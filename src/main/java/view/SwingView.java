package view;

import javax.swing.JFrame;

import control.CommandListener;
import model.World;

/**
 * Created by asazonov on 17.07.2018.
 */
public class SwingView implements View {

    private JWorldView view = new JWorldView();

    @Override
    public void init(World world) {
        view.setWorld(world);

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(view);
        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public void repaint() {
        view.repaint();
    }

    public void addCommandListener(CommandListener listener) {

    }

}
