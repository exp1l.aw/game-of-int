package model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Created by asazonov on 17.07.2018.
 */
@Getter
@Setter
@RequiredArgsConstructor
public class WorldObject {
    private final int id;

    private Point position;

    private String skin;

    public void onTick() {
    }
}
