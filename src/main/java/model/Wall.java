package model;

/**
 * Created by asazonov on 16.07.2018.
 */
public class Wall extends WorldObject {
    public Wall(int id) {
        super(id);

        setSkin("█");
    }
}
