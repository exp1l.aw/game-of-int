package model;

/**
 * Created by asazonov on 17.07.2018.
 */
public class WorldFactory {
    public World createWorld(int width, int height) {
        return new World(width, height);
    }
}
