package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by asazonov on 17.07.2018.
 */
@RequiredArgsConstructor
public class World {
    @Getter
    private final int width;
    @Getter
    private final int height;

    private final List<WorldObject> objects = new ArrayList<>();

    public List<WorldObject> getObjects() {
        return Collections.unmodifiableList(objects);
    }

    public void placeObject(WorldObject object, Point position) {
        if (isPointEngaged(position)) {
            throw new IllegalStateException("point engaged");
        }

        object.setPosition(position);
        objects.add(object);
    }

    public void move(int objectId, Point newPosition) {

    }

    public boolean isPointEngaged(Point point) {
        return objects.stream()
                .anyMatch(it -> it.getPosition().equals(point));
    }
}
