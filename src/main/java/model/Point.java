package model;

import lombok.Data;

/**
 * Created by asazonov on 16.07.2018.
 */
@Data
public class Point {
    private final int x;
    private final int y;
}
