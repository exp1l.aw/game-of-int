package model.creature;

import control.ai.PlayerAI;
import model.creature.template.PlayerTemplate;

public class Player extends Creature {
    public Player(int id, PlayerTemplate template) {
        super(id, template);
    }

    @Override
    public PlayerAI getAi() {
        return (PlayerAI) super.getAi();
    }
}
