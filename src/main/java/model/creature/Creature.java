package model.creature;

import control.ai.CreatureAI;
import lombok.Getter;
import lombok.Setter;
import model.WorldObject;
import model.creature.template.CreatureTemplate;

/**
 * Created by asazonov on 17.07.2018.
 */
@Getter
@Setter
public class Creature extends WorldObject {
    private CreatureAI ai = CreatureAI.DUMB;

    private final CreatureTemplate template;

    private int health;

    public Creature(int id, CreatureTemplate template) {
        super(id);
        this.template = template;
        init();
    }

    private void init() {
        setSkin(template.getSkin());
        setHealth(template.getHealth());
    }

    @Override
    public void onTick() {
        ai.onTick();
    }
}
