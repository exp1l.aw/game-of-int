package model.creature.template;

/**
 * Created by asazonov on 17.07.2018.
 */
public class PlayerTemplate extends CreatureTemplate {
    public static PlayerTemplate NORMAL = new PlayerTemplate() {{
        setSkin("P");
        setHealth(100);
    }};
}
