package model.creature.template;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by asazonov on 17.07.2018.
 */
@Getter
@Setter
public class CreatureTemplate {
    private String skin = "�";
    private int health;
}
