package control;

/**
 * Created by asazonov on 17.07.2018.
 */
public interface CommandListener {
    void onCommand(String cmd);
}
