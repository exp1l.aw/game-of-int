package control;

import control.ai.PlayerAI;
import lombok.Getter;
import model.creature.Player;
import model.Point;
import model.Wall;
import model.World;
import model.WorldFactory;
import model.WorldObject;
import model.creature.template.PlayerTemplate;

/**
 * Created by asazonov on 17.07.2018.
 */
public class God implements CommandListener {

    private final IdFactory idFactory = new IdFactory();
    private final WorldFactory worldFactory = new WorldFactory();

    @Getter
    private World world;

    public World createWorld(int width, int height) {
        if (world != null) {
            throw new IllegalStateException("World already created");
        }
        world = worldFactory.createWorld(width, height);

        initWorld();

        return world;
    }

    public Player createPlayer(PlayerTemplate template) {
        Player player = new Player(idFactory.nextId(), template);
        player.setAi(new PlayerAI());
        world.placeObject(player, new Point(1, 1));

        return player;
    }

    @Override
    public void onCommand(String cmd) {
        tick();
    }

    private void tick() {
        world.getObjects().forEach(WorldObject::onTick);
    }

    private void initWorld() {
        createBorder();
    }

    private void createBorder() {
        for (int i = 1; i < world.getWidth() - 1; i++) {
            createWall(i, 0);
            createWall(i, world.getHeight() - 1);
        }
        for (int i = 0; i < world.getHeight(); i++) {
            createWall(0, i);
            createWall(world.getWidth() - 1, i);
        }
    }

    private void createWall(int x, int y) {
        Wall wall = new Wall(idFactory.nextId());
        world.placeObject(wall, new Point(x, y));
    }


}
