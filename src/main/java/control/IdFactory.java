package control;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by asazonov on 17.07.2018.
 */
public class IdFactory {
    private AtomicInteger generator = new AtomicInteger();

    public int nextId() {
        return generator.incrementAndGet();
    }
}
