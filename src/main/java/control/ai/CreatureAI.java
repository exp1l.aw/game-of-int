package control.ai;

/**
 * Created by asazonov on 17.07.2018.
 */
public abstract class CreatureAI {
    public static CreatureAI DUMB = new CreatureAI() {
        @Override
        public void onTick() {
        }
    };

    public abstract void onTick();
}
